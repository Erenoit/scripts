# Erenoit's Scripts

These are the scripts that I used in my system

It consists of 3 categories:
1. Menu scripts for dmenu/rofi
2. Statusbar scripts for any statusbar (I use [dwmblocks](https://github.com/torrinfail/dwmblocks))
3. Other scripts for general use

### Note:
You need to add them to your `$PATH` to use.

## Dependencies
- The tools that most Linux distributions have by default:
    - aria2c
    - awk
    - cat
    - cd
    - chmod
    - curl
    - date
    - echo
    - find
    - free
    - grep
    - head
    - mount
    - printf
    - ps
    - read
    - rfkill
    - sensors
    - sleep
    - sort
- Others:
    - aura
    - bluetoothctl
    - pavucontrol
    - prime-run
    - pulsemixer
    - OpenTabletDriver
- The apps that `autostart` runs
